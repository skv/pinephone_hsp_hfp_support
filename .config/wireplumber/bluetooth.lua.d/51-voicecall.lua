bluez_monitor.properties = {
  ["bluez5.enable-msbc"] = false,


  -- HFP/HSP hardware offload SCO support (default: false).
  ["bluez5.hw-offload-sco"] = true,
}
