1. Copy content from ucm2 to /usr/share/alsa/ucm2/
1. Install wireplumber (0.4.13 or above), pipewire (0.3.62 or above) with necessary changes for SCO HW offload
1. copy bin to ~/bin
1. copy .local to ~/.local
1. copy .config to ~/.config
1. reboot
1. Connect HFP-capable Bluetooth headset.
1. Make a call
1. During a call launch BluetoothCall application from launcher. Call will be routed to Bluetooth headset.

Every time BluetoothCall is launched. It switches routing between 'earpiece/wired headset' and Bluetooth headset. If the "app" is launched without any call. It changes Bluetooth headset to A2DP profile.
