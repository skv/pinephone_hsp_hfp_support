#!/bin/sh
#set -x

BT_HFP_PROFILE=headset-head-unit
BT_A2DP_PROFILE=a2dp-sink

set_hifi_bt_hfp()
{
	callaudiocli -m 0
	pactl set-card-profile "$BT_PA_CARD" $BT_HFP_PROFILE
	pactl set-source-port alsa_input.platform-sound.HiFi__hw_PinePhone_0__source '[In] BluetoothHeadset'
	pactl set-sink-port alsa_output.platform-sound.HiFi__hw_PinePhone_0__sink '[Out] BluetoothHeadset'
	#pactl set-sink-volume alsa_output.platform-sound.HiFi__hw_PinePhone_0__sink 40%
	pw-cli set-param "$BT_PW_ID" 2 '{ bluetoothOffloadActive = true }'
}

set_hifi_bt_a2dp()
{
	callaudiocli -m 0
	if [ -n "$BT_PW_ID" ]; then
		pw-cli set-param "$BT_PW_ID" 2 '{ bluetoothOffloadActive = false }'
	fi
	if [ -n "$BT_PA_CARD" ]; then
		pactl set-card-profile "$BT_PA_CARD" $BT_A2DP_PROFILE
	fi
	pactl set-source-port alsa_input.platform-sound.HiFi__hw_PinePhone_0__source '[In] DigitalMic'
	pactl set-sink-port alsa_output.platform-sound.HiFi__hw_PinePhone_0__sink '[Out] Earpiece'

	if [ -n "$BT_PA_CARD" ]; then
		BT_PA_SINK=$(pactl list sinks | grep -e 'Name: ' -e  $BT_A2DP_PROFILE | grep -v 'device.profile' | grep -B 1 -e $BT_A2DP_PROFILE | grep 'Name:' | sed 's/[<>]//g' | awk '{print $2}')
		pactl set-default-sink "$BT_PA_SINK"
	fi
}

set_hifi_phone()
{
	callaudiocli -m 0
	set_hifi_bt_a2dp
	pactl set-default-sink alsa_output.platform-sound.HiFi__hw_PinePhone_0__sink
	pactl set-default-source alsa_input.platform-sound.HiFi__hw_PinePhone_0__source
}

set_voicecall_bt()
{
	callaudiocli -m 1
	pactl set-card-profile "$BT_PA_CARD" $BT_HFP_PROFILE
	pactl set-source-port alsa_input.platform-sound.Voice_Call__hw_PinePhone_0__source '[In] BluetoothHeadset'
	pactl set-sink-port alsa_output.platform-sound.Voice_Call__hw_PinePhone_0__sink '[Out] BluetoothHeadset'
	pactl set-sink-volume alsa_output.platform-sound.Voice_Call__hw_PinePhone_0__sink 40%
	pw-cli set-param "$BT_PW_ID" 2 '{ bluetoothOffloadActive = true }'
}


set_voicecall_phone()
{
	callaudiocli -m 1
	pactl set-card-profile "$BT_PA_CARD" $BT_A2DP_PROFILE
	pw-cli set-param "$BT_PW_ID" 2 '{ bluetoothOffloadActive = false }'
	pactl set-source-port alsa_input.platform-sound.Voice_Call__hw_PinePhone_0__source '[In] DigitalMic'
	pactl set-sink-port alsa_output.platform-sound.Voice_Call__hw_PinePhone_0__sink '[Out] Earpiece'
}

show_message()
{
	MSG=$1
	echo "$MSG"
	notify-send -e Sound "$MSG"
}

show_settings()
{
	MSG=$1
	PROFILE=$(pactl list cards | grep -e 'Name' -e 'Active Profile' | grep -A 1 'alsa_card.platform-sound' | grep 'Active Profile' | awk '{print $3}')
	SINK_PORT=$(pactl list sinks | grep -e 'Name' -e 'Active Port' | grep -A 1 'platform-sound' | grep 'Active Port' | sed 's/^.*Active Port: //')
	SOURCE_PORT=$(pactl list sources | grep -e 'Name' -e 'Active Port' | grep -A 1 'platform-sound' | grep 'Active Port' | sed 's/^.*Active Port: //')
	CALL_MODE=$(callaudiocli -S | grep 'Selected mode')

	show_message "$MSG\n$PROFILE: $SINK_PORT, $SOURCE_PORT\n$CALL_MODE"
}

set_phone()
{
	if [ -z "$IN_CALL" ]; then
		if [ -z "$BT_PA_CARD" ]; then
			set_hifi_phone
			show_settings "Switch to phone"
		else
			set_hifi_bt_a2dp
			show_settings "Switch to A2DP BT"
		fi
	else
		set_voicecall_phone
		show_settings "Switch to phone (call)"
	fi
}

set_bt()
{
	if [ -z "$IN_CALL" ]; then
		set_hifi_bt_hfp
		show_settings "Switch to HFP/HSP BT"
	else
		set_voicecall_bt
		show_settings "Switch to HFP/HSP BT (call)"
	fi
}

IN_CALL=$(dbus-send --session --print-reply --type=method_call --dest=org.gnome.Calls /org/gnome/Calls/Call org.freedesktop.DBus.Introspectable.Introspect | grep 'node name=')

# Check for connected bluetooth headset
BT_PA_CARD=$(pactl list cards | grep -e 'Name: ' -e  $BT_HFP_PROFILE | grep -B 1 -e $BT_HFP_PROFILE | grep 'Name:' | sed 's/[<>]//g' | awk '{print $2}')
if [ -z "$BT_PA_CARD" ]; then
	show_message "Bluetooth card with $BT_HFP_PROFILE is not found"
	paplay /usr/share/sounds/freedesktop/stereo/suspend-error.oga
	set_phone
	exit 1
fi

# Check for connected bluetooth headset and a2dp profile. 
# Otherwise switching is not working
BT_PA_CARD=$(pactl list cards | grep -e 'Name: ' -e  $BT_A2DP_PROFILE | grep -v 'device.profile' | grep -B 1 -e 'available' | grep 'Name:' | sed 's/[<>]//g' | awk '{print $2}')
if [ -z "$BT_PA_CARD" ]; then
	show_message "Bluetooth card with $BT_A2DP_PROFILE is not found"
	paplay /usr/share/sounds/freedesktop/stereo/suspend-error.oga
	set_phone
	exit 1
fi

BT_PW_ID=$(pactl list cards | grep -e "Name: $BT_PA_CARD" -e 'object.id'  | grep -A 1 -e "$BT_PA_CARD" | grep -e 'object.id' | sed 's/[<>]//g' | sed 's/"//g' | awk '{print $3}')
echo "Bluetooth card is $BT_PA_CARD ($BT_PW_ID)"


HFP_TEST=$(pactl list cards | grep 'Active Profile:' | grep $BT_HFP_PROFILE)
if [ -z "$HFP_TEST" ]; then
	set_bt
else
	set_phone
fi

